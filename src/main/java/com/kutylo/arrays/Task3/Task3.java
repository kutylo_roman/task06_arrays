package com.kutylo.arrays.Task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;

public class Task3 implements Comparable<Task3> {
    public static Logger logger = LogManager.getLogger(Task3.class);
    String firstString;
    String secondString;

    Task3(){
        initCountry();
    }
   private static String[][] data = {
            {"Kyiv", "Ukraine"},
            {"Turkey", "Ankara"},
            {"Australia", "Canberra"},
            {"Cuba", "Havana"},
            {"Monaco", "Monaco"},
            {"Italy", "Rome"},
    };

    @Override
    public int compareTo(Task3 o) {
        //compare by city name
        return o.secondString.compareTo(this.secondString);
    }

//
//    @Override
//    public int compareTo(Task3 o) {
//        //compare by country name
//        return o.firstString.compareTo(this.firstString);
//    }



    public void initCountry(){

        int num = (int)(Math.random()*6);

        firstString = (data[num][0]);
        secondString = data[num][1];
    }

    @Override
    public String toString() {
        return firstString + " " + secondString;
    }

    public static void main(String[] args) {
        ArrayList<Task3> arrayList = new ArrayList<>();
        for(int i =0; i<20; i++){
            Task3 tmp = new Task3();
            tmp.initCountry();
            arrayList.add(tmp);
        }
        logger.info(Arrays.toString(arrayList.toArray()));
        arrayList.sort(Task3::compareTo);
        logger.info(Arrays.toString(arrayList.toArray()));
    }
}
