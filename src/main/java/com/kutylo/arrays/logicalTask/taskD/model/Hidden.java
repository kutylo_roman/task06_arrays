package com.kutylo.arrays.logicalTask.taskD.model;

public abstract class Hidden {
    protected boolean isActive;
    protected int hp;
    protected final int MIN_HP;
    protected final int MAX_HP;
    Hidden(int min_hp, int max_hp){
        MAX_HP = max_hp;
        MIN_HP = min_hp;
        hp = MIN_HP + (int) (Math.random() * (MAX_HP-MIN_HP));
        isActive = true;
    }
    public abstract int interuct();

    public void setActive(boolean active) {
        isActive = active;
    }
}
