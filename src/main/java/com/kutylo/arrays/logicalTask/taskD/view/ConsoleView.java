package com.kutylo.arrays.logicalTask.taskD.view;

import com.kutylo.arrays.logicalTask.taskD.controller.Game;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView {
    public static Logger logger = LogManager.getLogger(ConsoleView.class);
    Game game = new Game();

    public void view() {
        logger.info(game.getInfo());
        viewDeadDoors();
        viewSurviveDoors();
    }

    public void viewDeadDoors() {
        logger.info("Doors of dead number: " + game.calculateDeadDoors());
    }

    public void viewSurviveDoors(){
        logger.info(game.surviveDoorsArray());
    }
}
