package com.kutylo.arrays.logicalTask.taskD.model;

public class Hero {
    final int START_HP = 25;
    private int hp;

    public Hero() {
        this.hp = START_HP;
    }

    public int updateHP(Door door){
        hp+=door.open();
        return hp;
    }

    public int getHp() {
        return hp;
    }
}
