package com.kutylo.arrays.logicalTask.taskD;

import com.kutylo.arrays.logicalTask.taskD.view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        new ConsoleView().view();
    }
}
