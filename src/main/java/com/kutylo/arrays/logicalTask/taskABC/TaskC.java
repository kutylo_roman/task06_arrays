package com.kutylo.arrays.logicalTask.taskABC;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * Знайти в масиві всі серії однакових елементів, які йдуть підряд,
 * і видалити з них всі елементи крім одного.
 */
public class TaskC {
    public static Logger logger = LogManager.getLogger(TaskC.class);
    public static int[] masuv;

    TaskC() {
        masuv = new int[20];
//        for (int i = 0; i < masuv.length; i++) {
//            masuv[i] = (int) (Math.random() * 100);
//        }
        masuv[0] = 5;
        masuv[1] = 6;
        masuv[2] = 7;
        masuv[3] = 5;
        masuv[4] = 6;
        masuv[5] = 7;
        masuv[6] = 11;
        masuv[7] = 5;
        masuv[8] = 6;
        masuv[9] = 12;
        masuv[10] = 11;
        masuv[11] = 5;
        masuv[12] = 6;
        masuv[13] = 12;
        masuv[14] = 6;
        masuv[15] = 9;
        masuv[16] = 3;
        masuv[17] = 6;
        masuv[18] = 9;
        masuv[19] = 3;

    }

    public void clearAll() {
        for (int i = masuv.length / 2 - 1; i >= 2; i--) {
            clear(i);
        }
    }

    private int clear(int length) {

        for (int j = 0, i = 0; j < masuv.length - length; j++) {
            for (i = 0; i < length && i+j+length<masuv.length ; i++) {
                if (masuv[i + j] != masuv[length + i + j]) {
                    break;
                }
                if (i == length - 1) {
                    removeSerie(length, i + j - length);
                    removeSerie(length, i + j - length + 1);
                }
            }
            continue;
        }
        return masuv.length;
    }

    private void removeSerie(int length, int startIndex) {
        for (int k = 1; k < length; k++) {
            remove(startIndex + k);
        }
    }

    public void remove(int index) {
        int[] newMasuv = new int[masuv.length - 1];

        for (int i = 0, j = 0; i < masuv.length; i++) {
            if (i != index) {
                newMasuv[j] = masuv[i];
                j++;
            }
        }
        masuv = newMasuv;
    }


    public static void main(String[] args) {
        TaskC taskC = new TaskC();
        logger.info(Arrays.toString(masuv));
        taskC.clearAll();
        logger.info(Arrays.toString(masuv));
    }
}


