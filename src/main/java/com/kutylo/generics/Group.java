package com.kutylo.generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Write your generic class – container with units.
 * You can put and get units from container.
 * Try to implement it using wildcards.
 */

public class Group<T> {
    private List<? super T> metals = new ArrayList<>();

    public void addElement(T element) {
        metals.add(element);
    }

    public int getLength(){
        return metals.size();
    }
    public T getElements(int i) {
        return (T)metals.get(i);
    }
}
