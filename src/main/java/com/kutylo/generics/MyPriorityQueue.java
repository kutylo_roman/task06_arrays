package com.kutylo.generics;
import java.util.*;

/**
 * Write your PriorityQueue class using generics.
 * @param <T> generic type for priority queue
 */
public class MyPriorityQueue<T> implements Queue {

    List<T> myQueue;
    Comparator<T> comparator;

    MyPriorityQueue(Comparator<T> comparator) {
        myQueue = new ArrayList<>();
        this.comparator = comparator;
    }


    private int findHeadNumber() {
        int headNumber = 0;
        for (int i = 0; i < size(); i++) {
            if (comparator.compare(myQueue.get(i), myQueue.get(headNumber)) > 0) headNumber = i;
        }
        return headNumber;
    }

    @Override
    public boolean add(Object o) {
        if (myQueue.add((T) o)) return true;
        else return false;
    }

    @Override
    public boolean remove(Object o) {
        return myQueue.remove(o);
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public int size() {
        return myQueue.size();
    }

    @Override
    public Object peek() {
        return myQueue.get(findHeadNumber());
    }

    @Override
    public Object poll() {
        if (size() == 0) return null;
        int headNumber = findHeadNumber();
        Object obj = myQueue.get(headNumber);
        myQueue.remove(headNumber);
        return obj;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) return true;
        else return false;
    }

    @Override
    public Object remove() {
        return null;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }


    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }


    @Override
    public boolean offer(Object o) {
        return false;
    }


    @Override
    public Object element() {
        return null;
    }


}
